from tastypie.resources import ModelResource
from directory.models import Empresa

class EmpresaResource(ModelResource):
    class Meta:
        queryset = Empresa.objects.all()
        resource_name = "empresa"
