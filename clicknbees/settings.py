"""
Django settings for clicknbees project.

For more information on this file, see
https://docs.djangoproject.com/en/1.6/topics/settings/

For the full list of settings and their values, see https://docs.djangoproject.com/en/1.6/ref/settings/
"""

import os
# Build paths inside the project like this: os.path.join(BASE_DIR, ...) import os
BASE_DIR = os.path.dirname(os.path.dirname(__file__))

env = "dev"

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.6/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '&5-saxmf#2c%fqdoru@e4=s%qlnr@xd8xp-@x^wg92j^k+wa2&'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

TEMPLATE_DEBUG = True

ALLOWED_HOSTS = []


# Application definition

INSTALLED_APPS = (
    'grappelli', # Admin Site style
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites', # This must be included to use allauth
    #'south', # Migrations
    'directory', # Main App
    'perfil', # User Profile
    'dependencias',
    'landing',

    # Additional libraries
    'geoposition',
    'widget_tweaks',
    'randomslugfield',
    #'rest_framework',
    'tastypie',
    'corsheaders',

    # Django-Allauth applications
    'allauth',
    'allauth.account',
    'allauth.socialaccount',
    'allauth.socialaccount.providers.facebook',
    'allauth.socialaccount.providers.google',
    'allauth.socialaccount.providers.twitter',

)

SITE_ID = 1

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = 'clicknbees.urls'

WSGI_APPLICATION = 'clicknbees.wsgi.application'


# Database
# https://docs.djangoproject.com/en/1.6/ref/settings/#databases

DATABASES = {
    'default': {
        #'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(BASE_DIR, 'db.sqlite3'),
        #'NAME': 'postgres://hcxwstjnukermr:H2ZM_DqWgh81bpjaZk2755kPsc@ec2-54-225-168-181.compute-1.amazonaws.com:5432/d8fkrev7jrkcus',


    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.6/topics/i18n/

LANGUAGE_CODE = 'es-mx'

TIME_ZONE = 'UTC'

USE_I18N = True

USE_L10N = True

USE_TZ = True


# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.6/howto/static-files/

STATIC_URL = '/static/'

STATICFILES_DIRS = (
    'static',
)

TEMPLATE_DIRS = (
    'templates',
)

MEDIA_ROOT = "media"
MEDIA_URL = "/media/"

# Configuration for Django-Allauth
TEMPLATE_CONTEXT_PROCESSORS = (
    'django.core.context_processors.request',
    'allauth.account.context_processors.account',
    'allauth.socialaccount.context_processors.socialaccount',
    'django.contrib.auth.context_processors.auth',
)

AUTHENTICATION_BACKENDS = (
    'django.contrib.auth.backends.ModelBackend',
    'allauth.account.auth_backends.AuthenticationBackend',
)

SOCIAL_ACCOUNT_PROVIDERS = {
   'facebook': {
        'SCOPE': ['email', 'publish_stream'],
        'METHOD': 'js_sdk' # instead of OAuth2
    },
    'google': {
        'SCOPE': ['https://www.googleapis.com/auth/userinfo.profile',
                  'https://www.googleapis.com/auth/userinfo.email'],
        'AUTH_PARAMS': {'access_type': 'online'}
    }
}

ACCOUNT_EMAIL_REQUIRED = True

# End Django-Allauth Configuration

# Enable this to test email processing through the console
#EMAIL_BACKENDS = 'django.core.mail.backends.console.EmailBackend'

LOGIN_REDIRECT_URL = '/'

# Email credentials configuration
EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = 587
EMAIL_HOST_USER = 'aadelgado@fevaq.net'
EMAIL_HOST_PASSWORD = 'c8B0 r&c#c'
#EMAIL_BACKEND = "django.core.mail.backends.smtp.EmailBackend"

APPEND_SLASH = True


# Heroku configuration
if env == "prod":
    import dj_database_url
    DATABASES["default"] = dj_database_url.config()
    SECURE_PROXY_SSL_HEADER = ("HTTP_X_FORWARDED_PROTO", "https")
    ALLOWED_HOSTS = ["*"]


import os
BASE_DIR = os.path.dirname(os.path.abspath(__file__))
STATIC_ROOT = "static"

STATICFILES_DIRS = (
    os.path.join(BASE_DIR, "static"),
)

GRAPPELLI_ADMIN_TITLE = "ADMINISTRADOR DE CLICKNBEES"

CLOUDINARY = {
    "cloud_name": "dumbellhero",
    "api_key": "432851373335487",
    "api_secret": "-UaL4byep0tb03a3vx1IKbFkwjI",
}


TASTYPIE_DEFAULT_FORMATS = ['json']

# ALLAUTH CONFIGURATION
ACCOUNT_EMAIL_REQUIRED = True
ACCOUNT_EMAIL_CONFIRMATION_EXPIRE_DAYS = 7
ACCOUNT_EMAIL_VERIFICATION = "mandatory"

# Cors configuration
MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
    'corsheaders.middleware.CorsMiddleware',
    'django.middleware.common.CommonMiddleware',
)

CORS_ORIGIN_ALLOW_ALL = True


