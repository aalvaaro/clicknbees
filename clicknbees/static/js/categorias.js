GIROS = (("alimentos", "Alimentos"), ("entretenimiento", "Entretenimiento"),
 10         ("educacion", "Educacion"), ("social", "Social"), ("deportes", "Deportes"),
 11         ("gubernamental", "Gubernamental"), ("tecnologia", "Tecnología"),
 12         ("comercio", "Comercio"), ("moda", "Moda y Accesorios"), ("transporte", "Transporte"), ("profesional", "Profesional"),
 13         ("asociaciones", "Asociaciones"), ("turismo", "Turismo"), ("servicios", "Servicios Varios"),
 14         ("hogar", "Hogar"), ("financieras", "Financieras"),
 15         ("salud", "Salud"), ("oficios", "Oficios"), ("eventos", "Eventos"), ("proveedores", "Proveedores"))
 16 
 17 # Subcategories correponding to each category
 18 ALIMENTOS = (("mexicana", "Mexicana"), ("italiana", "Italiana"), ("sudamericana", "Sudamericana"),
 19              ("rapida", "Rápida"), ("oriental", "Oriental"), ("internacional", "Internacional"))
 20 ENTRETENIMIENTO = (("cybercafe", "CyberCafé"), ("cine", "Cine"), ("teatro", "Teatro"), ("otro", "Otro"))
 21 EDUCACION = (("escuela", "Escuela"), ("uniformes", "Uniformes")) 
 22 SOCIAL = (("bar", "Bar"), ("cafe", "Café"), ("nocturno", "Club Nocturno"), ("otro", "Otro"))
 23 DEPORTES = (("canchas", "Canchas"), ("equipo", "Equipo Deportivo"), ("gimnasio", "Gimnasio"), ("club", "Club Deportivo"))
 24 TECNOLOGIA = (("computadoras", "Computadoras"), ("celulares", "Celulares"), ("videojuegos", "Videojuegos"))
 25 COMERCIO = (("supermercado", "Supermercado"), ("naturista", "Naturista"), ("mercado", "Mercado"), ("tienda", "Tienda"),
 26             ("vinateria", "Vinatería"), ("abarrotes", "Abarrotes"), ("dulceria", "Dulcería"), ("ferreteria", "Ferretería"))
 27 MODA = (("calzado", "Calzado"), ("ropa", "Ropa"), ("joyeria", "Joyería"), ("optica", "Óptica"), ("perfumeria", "Perfumería"),
 28         ("belleza", "Productos de Belleza"), ("esteticas", "Estéticas"))
 29 TRANSPORTE = (("taxis", "Taxis"), ("publico", "Transporte Público"), ("renta", "Renta"), ("autobuses", "Autobuses"), ("aeropuertos", "Aeropuertos"))
 30 PROFESIONAL = (("abogado", "Abogado"), ("contador", "Contador"), ("serigrafia", "Serigrafía"), ("diseno", "Diseño"), ("belleza", "Belleza y Estética"))
 31 ASOCIACIONES = (("tercera", "Tercera Edad"), ("ninos", "Niños"), ("animales", "Animales"), ("mujer", "Mujer"), 
 32                 ("ambiente", "Medio Ambiente"), ("derechos", "Derechos Humanos"))
 33 TURISMO = (("hotel", "Hotel"), ("agencia", "Agencia de Viajes"), ("autobuses", "Autobuses"))
 34 SERVICIOS = (("funeraria", "Funeraria"), ("limpieza", "Limpieza"), ("seguridad", "Seguridad Privada"), ("imprenta", "Imprenta y Serigrafía"),
 35              ("paqueteria", "Paquetería y Fletes"))
 36 HOGAR = (("muebles", "Muebles"), ("utensilios", "Utensilios"), ("pintura", "Pintura"), ("jardineria", "Jardinería"), 
 37          ("construccion", "Construcción"), (("bienes", "Bienes Raíces"))
 38 FINANCIERAS = (("caja", "Caja"), ("banco", "Banco"), ("empeno", "Empeño"), ("prestamos", "Préstamos"))
 39 SALUD = (("doctor", "Doctor"), ("dentista", "Dentista"), ("psicologo", "Psicólogo"), ("clinica", "Clínica"), ("hospital", "Hospital"), ("equipo", "Equipo Médico"),
 40          ("uniformes", "Uniformes"), ("estancia", "Estancia Infantil"), ("tercera", "Tercera Edad"), ("alternativa", "Medicina Alternativa"))
 41 OFICIOS = (("plomero", "Plomero"), ("carpintero", "Carpintero"), ("pintor", "Pintor"), ("electricista", "Eléctricista"), ("jardinero", "Jardinero"))
 42 EVENTOS = (("salon", "Salón"), ("accesorios", "Accesorios"), ("banquetes", "Banquetes"), ("sonido", "Sonido"), ("rocolas", "Rocolas"), 
 43            ("inflables", "Inflables"), ("pasteles", "Pasteles"), ("organizacion", "Organización de Eventos"))
