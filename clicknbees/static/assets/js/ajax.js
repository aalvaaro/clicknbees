$(function() {
	$('#search').keyup(function() {		
		$.ajax({			
			type: "POST",
			url: "/search/",
			data: {
				"search_text": $(this).val(),
				"csrfmiddlewaretoken": $('input[name=csrfmiddlewaretoken]').val()
			},
			success: searchSuccess,
			dataType: "html"
		});
	});

	$('.filtrar').click(function() {
		$.ajax({
			type: "POST",
			url: "/filtrar/",
			data: {
				"filtrar": $(this).attr("value"),
				"csrfmiddlewaretoken": $('input[name=csrfmiddlewaretoken]').val()
			},
			success: filterSuccess,
			dataType: "html"
		});
	});
});


function searchSuccess(data, textStatus, jqXHR)
{
	console.log(data);
	$('#search_results').html(data);
}

function filterSuccess(data, textStatus, jqXHR)
{	
	console.log("Filtering..")
	//console.log(data);
	$('#filter_results').html(data);

}