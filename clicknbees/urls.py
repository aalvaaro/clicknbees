from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from directory.api import EmpresaResource
from tastypie.api import Api

api = Api(api_name = "v1")
api.register(EmpresaResource())


# MEDIA upload imports
from django.conf import settings
from django.conf.urls.static import static

from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    url(r'^grappelli/', include('grappelli.urls')),
    url(r'^accounts/', include('allauth.urls')),
    url(r'^admin/', include(admin.site.urls)),

    # Directory URLS
       #(r'^$', TemplateView.as_view(template_name = "coral/index.html")),
    url(r'', include('directory.urls')),
    url(r'^perfil/', include('perfil.urls')),
    url(r'^dependencias/', include('dependencias.urls')),
    url(r'^about/', include('landing.urls')),
    #url(r'^beta/', include('landing.urls')),

    # API urls
    url(r'^api/', include(api.urls)),

    # New configuration
    url(r'^listado-empresas/$', TemplateView.as_view(template_name = "remake/lista.html"), name = "index"),
    #url(r'^dependencias/$', TemplateView.as_view(template_name = "remake/dependencias.html")),
    url(r'^featured/$', TemplateView.as_view(template_name = "remake/featured.html")),
    url(r'^registro-empresa/$', TemplateView.as_view(template_name = "remake/registro-empresa.html")),

    url(r'^beta/android-app/$', TemplateView.as_view(template_name = "remake/android.html")),

    url(r'^landing/', include('landing.urls')),




) + static(settings.MEDIA_URL, document_root = settings.MEDIA_ROOT)
