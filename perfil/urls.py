from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from .views import EditarUsuario, CrearEmpresa, EditarEmpresa

urlpatterns = patterns('',
    url(r'^(?P<pk>\d+)/$', TemplateView.as_view(template_name = 'perfil/index.html'), name = "perfil"),
    url(r'^(?P<pk>\d+)/editar/$', EditarUsuario.as_view(template_name = 'perfil/editar.html'), name="editar_perfil"),
    url(r'^crear/$', CrearEmpresa.as_view(), name = "crear_empresa"),
    url(r'^empresa/(?P<pk>\d+)/editar/$', EditarEmpresa.as_view(), name = "editar_empresa"),

)

