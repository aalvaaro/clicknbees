from django.shortcuts import render
from django.views.generic.edit import UpdateView, CreateView
from django.contrib.auth.models import User
from directory.models import Empresa
from directory.forms import EmpresaForm
from django.shortcuts import get_object_or_404
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse

class EditarUsuario(UpdateView):
    model = User
    fields = ["username", "first_name", "last_name", "email"]
    template_name = "perfil/editar.html"
    #success_url = "/perfil/"

    def get_success_url(self):
        return reverse('perfil', kwargs = {"pk": self.request.user.id})

class CrearEmpresa(CreateView):
    model = Empresa
    form_class = EmpresaForm
    template_name = "perfil/crear_empresa.html"
    success_url = "/perfil/"

    def get_success_url(self):
        return reverse('perfil', kwargs = {"pk": self.request.user.id})

    def form_valid(self, form):
        empresa = form.save(commit = False)
        empresa.user = User.objects.get(id = self.request.user.id)
        empresa.save()
        return HttpResponseRedirect(self.get_success_url())

class EditarEmpresa(UpdateView):
    model = Empresa
    form_class = EmpresaForm
    template_name = "perfil/editar_empresa.html"
    #$success_url = "/perfil/"

    def get_queryset(self):
        queryset = Empresa.objects.filter(pk = self.request.user.id)
        return queryset

    def get_success_url(self):
        return reverse('perfil', kwargs = {"pk": self.request.user.id})

