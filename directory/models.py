# -*- coding: utf-8 -*-

from django.db import models
from django.contrib.auth.models import User
from geoposition.fields import GeopositionField
from cloudinary.models import CloudinaryField
from autoslug import AutoSlugField
from randomslugfield import RandomSlugField

class Empresa(models.Model):
    user = models.ForeignKey(User, unique = True)
    nombre = models.CharField(max_length = 80, unique = True)
    slug = AutoSlugField(populate_from = "nombre", always_update = True, unique_with = "id")
    random_slug = RandomSlugField(length = 20)
    logotipo = models.URLField(null = True, blank = True)
    horarios = models.TextField(null = True, blank = True)
    giro = models.CharField(max_length = 40)
    categoria = models.CharField(max_length = 35, null = True, blank = True)
    descripcion = models.TextField(max_length = 400, null = True, blank = True)
    email = models.EmailField()
    telefono = models.CharField(max_length = 20, null = True, blank = True)
    celular = models.CharField(max_length = 15, null = True, blank = True)
    direccion = models.CharField(max_length = 100)
    colonia = models.CharField(max_length = 50)
    cp = models.IntegerField()

    # Social Accounts
    facebook = models.URLField(null = True, blank = True)
    twitter = models.URLField(null = True, blank = True)
    youtube = models.URLField(null = True, blank = True)

    def __unicode__(self):
        return self.nombre
