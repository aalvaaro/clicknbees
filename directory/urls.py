from django.conf.urls import patterns, include, url
from .views import Listado
from django.views.generic import TemplateView

urlpatterns = patterns('',
    url(r'^$', Listado.as_view(), name = "index"),

    # AJAX search url
    url(r'^search/$', 'directory.views.search'),
    url(r'^filtrar/$', 'directory.views.filtrar'),

)

