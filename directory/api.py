from tastypie.resources import ModelResource, ALL
from tastypie import fields
from .models import Empresa

class EmpresaResource(ModelResource):
    class Meta:
        queryset = Empresa.objects.all()
        resource_name = "empresas"
        list_allowed_methods = ["get"]
