#-*- coding: utf-8 -*-

from django import forms
from django.forms import ModelForm, TextInput, NumberInput, EmailInput, \
                        FileInput, Textarea
from .models import Empresa

class EmpresaForm(ModelForm):
    class Meta:
        model = Empresa
        exclude = ("slug", "user", "giro", "categoria")
        """widgets = {
            "nombre": TextInput(attrs = {"required": "true",
                                    "class": "form-control input-lg"}),
            "logotipo": FileInput(attrs = {"class": "margin-none"}),
            "slogan": TextInput(attrs = {"class": "form-control input-lg"}),
            "descripcion": Textarea(attrs = {"required": "true",
                                             "class": "",
                                             "placeholder": "Descripcion de tu negocio (máximo 200 caracteres)"}),
            "email": EmailInput(attrs = {"class": "form-control input-lg",
                                    "placeholder": "Email de tu empresa"}),
            "telefono": NumberInput(attrs = {"class": "form-control input-lg",
                                             "required": "true",
                                             "placeholder": "Telefono"}),
            "celular": NumberInput(attrs = {"class": "form-control input-lg",
                                            "placeholder": "Celular"}),
            "direccion": TextInput(attrs = {"class": "form-control input-lg",
                                            "placeholder": "Dirección: Calle y número"}),
            "colonia": TextInput(attrs = {"class": "form-control input-lg"}),
            "cp": NumberInput(attrs = {"class": "form-control input-lg"}),
        }"""



