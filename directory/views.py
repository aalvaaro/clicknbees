from django.shortcuts import render, render_to_response
from django.views.generic import ListView
from .models import Empresa

class Listado(ListView):
    model = Empresa
    context_object_name = "empresas"
    template_name = "remake/lista.html"

def search(request):
    if request.method == "POST":
        search_text = request.POST["search_text"]
        resultados = Empresa.objects.filter(nombre__icontains = search_text)
    else:
        resultados = Empresa.objects.all()
    context = {"resultados": resultados}
    return render_to_response("remake/ajax-search.html", context)

def filtrar(request):
    if request.method == "POST":
        giro = request.POST["filtrar"]
        resultados = Empresa.objects.filter(giro = giro)
    context = {"resultados": resultados}
    return render_to_response("remake/ajax-search.html", context)

