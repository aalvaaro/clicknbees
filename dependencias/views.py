from django.shortcuts import render
from django.views.generic import ListView
from .models import Oficina

class OficinaView(ListView):
    model = Oficina
    context_object_name = "dependencias"

