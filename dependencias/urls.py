from django.conf.urls import patterns, include, url
from django.views.generic import TemplateView
from .views import OficinaView

urlpatterns = patterns('',
    url(r'^$', OficinaView.as_view(template_name = "remake/dependencias.html"), name = "dependencias"),

)

