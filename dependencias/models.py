from django.db import models

class Oficina(models.Model):
    nombre = models.CharField(max_length = 50)
    direccion = models.CharField(max_length = 200)
    telefono = models.CharField(max_length = 15, null = True, blank = True)
    extensiones = models.CharField(max_length = 100, null = True, blank = True,
                                   help_text = "Cada extension seguida por una coma")
    logotipo = models.URLField()
    horario_atencion = models.CharField(max_length = 50)
    colonia = models.CharField(max_length = 80, null = True, blank = True)
    cp = models.CharField(max_length = 10, null = True, blank = True)
    ciudad = models.CharField(max_length = 80)
    estado = models.CharField(max_length = 80)
    facebook = models.URLField(null = True, blank = True)
    twitter = models.URLField(null = True, blank = True)
    youtube = models.URLField(null = True, blank = True)
    instagram = models.URLField(null = True, blank = True)

    def __unicode__(self):
        return self.nombre

