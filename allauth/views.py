from django.views.generic.edit import FormView
from .forms import MySignUpForm

SignupUser(FormView):
    form = MySignUpForm
