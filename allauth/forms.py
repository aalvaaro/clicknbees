#-*- coding: utf-8 -*-

from allauth.account.forms import SignupForm
from django import forms
from django.forms import TextInput, EmailInput, PasswordInput

class MySignUpForm(SignupForm):
    widgets = {
        "username": TextInput(attrs = {"required": "true",
                                       "class": "form-control input-lg"}),
        "email": EmailInput(attrs = {"required": "true",
                                     "class": "form-control input-lg"}),
        "password": PasswordInput(attrs = {"required": "true",
                                           "class": "form-control input-lg"})
    }



